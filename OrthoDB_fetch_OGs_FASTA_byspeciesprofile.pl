#!/usr/local/bin/perl
# Author: Robert M. Waterhouse (C) 2019 robert.waterhouse@unil.ch
# Example script for downloading Orthologous Group (OG) information and FASTA protein sequences for orthologous 
# groups selected with specific user-defined phyletic profiles from OrthoDB9 using the JSON API
# NB: only sequences from genes in orthologous groups that match the phyletic profile will be retrieved with this query
# For further details on using the API visit: http://www.orthodb.org/?page=api
# Please cite: OrthoDB v9.1: cataloging evolutionary and functional annotations for animal, fungal, plant, archaeal, bacterial and viral orthologs.
# Zdobnov EM, Tegenfeldt F, Kuznetsov D, Waterhouse RM, Simo FA, Ioannidis P, Seppey M, Loetscher A, Kriventseva EV.
# Nucleic Acids Res. 2017 Jan 4;45(D1):D744-D749. doi: 10.1093/nar/gkw1119. Epub 2016 Nov 28. PMID: 27899580

# RUN: perl OrthoDB_fetch_OGs_FASTA_byspeciesprofile.pl >& fetchlog_OGs_FASTA_byspeciesprofile.txt &

use strict;
use warnings;

###===###
# Make sure these required modules are installed:
use REST::Client;    # for database connection and querying
use JSON;            # for JSON to Perl interoperability

###===###
# Set connection to the OrthoDB website:
# Note https now for OrthoDB v10 connection
my $host = 'https://v100.orthodb.org/';
my $client = REST::Client->new(host => $host);

###===###
# User-defined taxonomic level:
my $level=7898;           # e.g. 7898 = NCBI taxonomy ID

###===###
# Set the species for which the results should be collected and printed
# The NCBI taxonomy ID must be from species at OrthoDB
# The names, e.g. 'Kmarm', can be set by the user
# Note for OrthoDB v10 taxonomy ID must have '_0' appended to work
my %selected = (
    '37003_0' => 'Kmarm',
    '105023_0' => 'Nfurz',
    '48699_0' => 'Plati',
    '8081_0' => 'Preti',
    '8090_0' => 'Olati',
    '48701_0' => 'Pmexi',
    '48698_0' => 'Pform',
    '8078_0' => 'Fhete',
    '52670_0' => 'Alimn',
    '28743_0' => 'Cvari',
    '8083_0' => 'Xmacu'
    );

###===###
# Set the required phyletic profile
# valid options, integers: 0, 1, 2, 3, 4, 5, 6, 7, 8, etc.
# valid options,  > or < : >0, >1, >2, >3, <4, <3, <2, etc.
# NB1: do not use >= or <= if you require <=1 then simply use <2 (as these are equivalent)
# NB2: all other species at the selected level will be ignored (i.e. copy-number=?)
# Note for OrthoDB v10 taxonomy ID must have '_0' appended to work
my %profile = (
    '37003_0' => '1',      
    '105023_0' => '1',      
    '48699_0' => '1',     
    '8081_0' => '1',      
    '8090_0' => '1',      
    '48701_0' => '<2',     
    '48698_0' => '<2',     
    '8078_0' => '<2',     
    '52670_0' => '<2',     
    '28743_0' => '<2',
    '8083_0' => '<2'    
    );

###===###
# Set the query to find all orthogroups at the specified taxonomic level
my $limit=1000000;            # this overrides the default limit of 1000
$client->GET("search?limit=$limit&level=$level");    
if($client->responseCode() !=200) { 
    print "Bad query: http://www.orthodb.org/search?limit=$limit&level=$level\n"; 
    print "Response Code: " . $client->responseCode() . "\n";
    print "Response Content: " . $client->responseContent() . "\n";
    exit(); 
}

###===###
# Convert JSON to PERL data structure
my $result = decode_json( $client->responseContent() );

###===###
# Get all orthologous groups returned from the query
my @groups = @{ $result->{'data'} };

###===###
# Check query details
print "START: " . `date`;
print "Your query: http://www.orthodb.org/search?limit=$limit&level=$level\n";
print "Returned " . scalar(@groups) . " orthologous groups\n\n";
print "Now searching for groups that match the following profile:\nTaxID\tCopy-number\n";
foreach my $taxid (sort keys %profile) { 
    if(defined($selected{$taxid})) { print "$taxid\t$profile{$taxid}\tSELECTED => $selected{$taxid}\n"; }
    else { print "$taxid\t$profile{$taxid}\n"; }
}
print "\nSearch progress:\n";

###===###
# Open filehandles to save the results:
# FASTA protein seqeunces for only the SELECTED species
open(FAS,">$level\_profile_selected\.fas") || die $!;
# List of orthologous group IDs of groups that match the profile
open(LST,">$level\_profile_selected\.txt") || die $!;
# List of orthologous group IDs of groups that do not match the profile
open(NON,">$level\_profile_rejected\.txt") || die $!;
# Additional information about the genes from the selected species in the groups that match the profile (in JSON format)
open(ANN,">$level\_profile_geneinfo\.txt") || die $!;

###===###
# Loop through each returned group to check for matches to the required profile
my $specieslist=join(",",(keys %profile));
my $genecount=0;
my $groupcount=0;
my $progressc=0;
my $orthogrpc=0;
my $sleepskip=0;
foreach my $group (@groups) {
    $client->GET("fasta?id=$group&species=$specieslist");
    if($client->responseCode() !=200) { 
	print "Bad query\n"; 
	print "Response Code: " . $client->responseCode() . "\n";
	print "Response Content: " . $client->responseContent() . "\n";
	exit(); 
    }
    if($client->responseContent()) { 
	my $fasta=$client->responseContent();
	my $profilematch='YES';
	my $fastaprint='';
	my $geneinfo='';

	undef my %gen2pep;
	undef my %gen2des;
	my @fastas=split(/\n>/,$fasta);
	foreach my $fas (@fastas) {
	    my @lines=split(/\n/,$fas);
	    my $id='';                                                # set ID to NULL
	    my $de='';                                                # set description to NULL
	    if($lines[0]=~/(\d+\:\S+)\s+(.+)/) { $id=$1; $de=$2; }    # save ID and description
	    $gen2pep{$id}=$lines[1];                                  # save ID with sequence
	    $de=~s/^{//;                                              # clean description
	    $de=~s/}$//;                                              # clean description 
	    $de=~s/\":\"/\"=\"/g;                                     # clean description
	    $gen2des{$id}=$de;                                        # save ID with description
	}

	# Loop through each species set in the required profile
	my $og_genecount=0;
	foreach my $species (keys %profile) {
	    # colect all genes from this species
	    my @genes=$fasta=~/>($species\:\S+)\s+/g;

	    # Check if the number of genes in this species matches that of the required profile
	    if($profile{$species}=~/>(\d+)/) {                                     # '>' filter
		my $copy=$1;
		if(scalar(@genes)<=$copy) { $profilematch='NO'; }
	    }
	    elsif($profile{$species}=~/<(\d+)/) {                                  # '<' filter
		my $copy=$1;
		if(scalar(@genes)>=$copy) { $profilematch='NO'; }
	    }
	    elsif(scalar(@genes)!=$profile{$species}) { $profilematch='NO'; }      # '=' filter

	    # If this species is one of the selected species then query OrthoDB for additional
	    # gene information and save the protein sequences in FASTA format for printing
	    if(defined($selected{$species})) { 
		foreach my $gen (sort @genes) {
		    $client->GET("ogdetails?id=$gen");
		    if($client->responseCode() !=200) { 
			print "Bad query\n"; 
			print "Response Code: " . $client->responseCode() . "\n";
			print "Response Content: " . $client->responseContent() . "\n";
			exit(); 
		    }
		    if($client->responseContent()) { 
			$geneinfo.=$group . "\t$selected{$species}\t$gen\t" . $client->responseContent() . "\n";
		    }
		    $fastaprint.=">$gen $selected{$species} $group $gen2des{$gen}\n$gen2pep{$gen}\n"; 
		}
		$og_genecount+=scalar(@genes);
	    }
	}

	# If this orthologous group matches the required profile then print the relevant data, or list non-matching groups
	if($profilematch eq 'YES') { 
	    print FAS $fastaprint; 
	    print LST "$group\n"; 
	    print ANN "$geneinfo";
	    $genecount+=$og_genecount;
	    $orthogrpc++;
	}
	else { print NON "$group\n"; }

	$progressc++;
	$groupcount++;
	if($progressc>=500) { # edit here to change how often to report progress
	    print "$genecount genes retrieved from $orthogrpc orthologous groups of $groupcount groups " . `date`;
	    $progressc=0; 
	}
    }
    
    $sleepskip++;
    if($sleepskip>10) {      # edit here to change how often and/or how long to pause between sets of queries
	my $cmd="sleep 1";   # pause between sets of queries to avoid bombarding OrthoDB with too many
	system($cmd);        # queries which could result in being locked out and disconnected
	$sleepskip=0;
    }
    
}

###===###
# Close filehandles
close(FAS);
close(LST);
close(NON);
close(ANN);
print "\nTotal genes retrieved: $genecount from $orthogrpc orthologous groups\n\nDONE: " . `date`;

