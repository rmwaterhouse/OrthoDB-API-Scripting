#!/usr/local/bin/perl
# Author: Robert M. Waterhouse (C) 2019 robert.waterhouse@unil.ch
# Example script for downloading Orthologous Group (OG) copy number information and evolutionary 
# rates for orthologous groups for user-selected species from OrthoDB9 using the JSON API
# For further details on using the API visit: http://www.orthodb.org/?page=api
# Please cite: OrthoDB v9.1: cataloging evolutionary and functional annotations for animal, fungal, plant, archaeal, bacterial and viral orthologs.
# Zdobnov EM, Tegenfeldt F, Kuznetsov D, Waterhouse RM, Simo FA, Ioannidis P, Seppey M, Loetscher A, Kriventseva EV.
# Nucleic Acids Res. 2017 Jan 4;45(D1):D744-D749. doi: 10.1093/nar/gkw1119. Epub 2016 Nov 28. PMID: 27899580

# RUN: perl OrthoDB_fetch_GENEEVOLUTION_byspecieslist.pl >& fetchlog_GENEEVOLUTION_byspecieslist.txt &

use strict;
use warnings;

###===###
# Make sure these required modules are installed:
use REST::Client;    # for database connection and querying
use JSON;            # for JSON to Perl interoperability

###===###
# Set connection to the OrthoDB website:
# Note https now for OrthoDB v10 connection
my $host = 'https://www.orthodb.org/';
my $client = REST::Client->new(host => $host);

###===###
# User-defined taxonomic level:
my $level=7898;           # e.g. 7898 = NCBI taxonomy ID for the node Atinopterygii

###===###
# Set the species for which the results should be collected and printed
# The NCBI taxonomy ID must be from species at OrthoDB
# The names, e.g. 'Agamb', can be set by the user
# Note for OrthoDB v10 taxonomy ID must have '_0' appended to work
my %selected = (
    '37003_0' => 'Kmarm',
    '105023_0' => 'Nfurz',   #
    '48699_0' => 'Plati',    #
    '8081_0' => 'Preti',     #
    '8090_0' => 'Olati',#
    '48701_0' => 'Pmexi',#
    '48698_0' => 'Pform',#
    '8078_0' => 'Fhete',#
    '52670_0' => 'Alimn',#
    '28743_0' => 'Cvari',#
    '8083_0' => 'Xmacu'   #   
    );


###===###
# Set the query to find all orthogroups at the specified taxonomic level
my $limit=100000;            # this overrides the default limit of 1000
$client->GET("search?limit=$limit&level=$level");    
if($client->responseCode() !=200) { 
    print "Bad query: http://www.orthodb.org/search?limit=$limit&level=$level\n"; 
    print "Response Code: " . $client->responseCode() . "\n";
    print "Response Content: " . $client->responseContent() . "\n";
    exit(); 
}

###===###
# Convert JSON to PERL data structure
my $result = decode_json( $client->responseContent() );

###===###
# Get all orthologous groups returned from the query
my @groups = @{ $result->{'data'} };

###===###
# Check query details
print "START: " . `date`;
print "Your query: http://www.orthodb.org/search?limit=$limit&level=$level\n";
print "Returned " . scalar(@groups) . " orthologous groups\n\n";
print "Now searching for gene evolutionary information ...\n";
print "\nSearch progress:\n";

###===###
# Open filehandle to save the results:
open(OUT,">$level\_geneevolution.txt") || die $!;
print OUT "GroupID\tSpecies\tOrthoDB_ID\tPublic_ID\t#Singlecopy\t#Multicopy\t#Present\t#TotalSpecies\t#TotalGenes\tEvolutionaryRate\tUniprot_ID\tDescription\n";

###===###
# Loop through each returned group to check for matches to the required profile
my $specieslist=join(",",(keys %selected));
my $genecount=0;
my $groupcount=0;
my $progressc=0;
my $orthogrpc=0;
my $sleepskip=0;
foreach my $group (@groups) {
    $client->GET("group?id=$group");
    if($client->responseCode() !=200) { 
	print "Bad query\n"; 
	print "Response Code: " . $client->responseCode() . "\n";
	print "Response Content: " . $client->responseContent() . "\n";
	exit(); 
    }

    # Convert JSON to PERL data structure
    my $groupinfo = decode_json( $client->responseContent() );

    # Collect Phyletic Profile data
    my $singlecopy=$groupinfo->{'data'}{'phyletic_profile'}{'single_copy'};
    my $totspecies=$groupinfo->{'data'}{'phyletic_profile'}{'species_count'};
    my $genescount=$groupinfo->{'data'}{'phyletic_profile'}{'gene_count'};
    my $multipcopy=$groupinfo->{'data'}{'phyletic_profile'}{'multi_copy'};
    my $presentspe=$groupinfo->{'data'}{'phyletic_profile'}{'present_in'};

    # Collect Evolutionary Rate
    my $evorate=$groupinfo->{'data'}{'evolutionary_rate'};

    # Query for the orthologs in the group from the selected species
    $client->GET("orthologs?id=$group&species=$specieslist");
    if($client->responseCode() !=200) { 
	print "Bad query\n"; 
	print "Response Code: " . $client->responseCode() . "\n";
	print "Response Content: " . $client->responseContent() . "\n";
	exit(); 
    }

    # Convert JSON to PERL data structure
    my $geneinfo = decode_json( $client->responseContent() );

    # Loop through each species in the group
    foreach my $species ( @{ $geneinfo->{'data'} } ) {
	my $species_name=$species->{'organism'}{'name'};

	# Get all genes for this species
	my @genes=@{$species->{'genes'}};
	foreach my $gene (@genes) {
	    my $odbid=$gene->{'gene_id'}{'param'};
	    my $pubid=$gene->{'gene_id'}{'id'};

	    # Collect UniProt ID and Description (if available)
	    my $uniprot='NA';
	    if(defined($gene->{'uniprot'}{'id'})) { $uniprot=$gene->{'uniprot'}{'id'}; }
	    my $desc='NA';
	    if(defined($gene->{'description'})) { $desc=$gene->{'description'}; }
	    
	    # Print results
	    print OUT "$group\t$species_name\t$odbid\t$pubid\t$singlecopy\t$multipcopy\t$presentspe\t$totspecies\t$genescount\t$evorate\t$uniprot\t$desc\n";

	    $genecount++;
	}

    }
    if(scalar( @{ $geneinfo->{'data'} } )>0) { $orthogrpc++; }
    
    $progressc++;
    $groupcount++;
    if($progressc>=250) { # edit here to change how often to report progress
	print "$genecount genes retrieved from $orthogrpc orthologous groups of $groupcount groups " . `date`;
	$progressc=0; 
    }
    
    $sleepskip++;
    if($sleepskip>10) {      # edit here to change how often and/or how long to pause between sets of queries
	my $cmd="sleep 1";   # pause between sets of queries to avoid bombarding OrthoDB with too many
	system($cmd);        # queries which could result in being locked out and disconnected
	$sleepskip=0;
    }
    
}

###===###
# Close filehandle
close(OUT);
print "\nTotal genes retrieved: $genecount from $orthogrpc orthologous groups\n\nDONE: " . `date`;

