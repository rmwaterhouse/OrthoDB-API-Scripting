#!/usr/bin/python3
# Author: Maarten JMF. Reijnders (C) 2019 maarten.reijnders@unil.ch
# Example script for downloading FASTA protein sequences from OrthoDB for a set of species using the JSON API
# NB: only sequences from genes in orthologous groups at the selected node will be retrieved with this query
# To fetch all sequences visit the flat file downloads page: http://www.orthodb.org/?page=filelist
# For further details on using the API visit: http://www.orthodb.org/?page=api
# Please cite: OrthoDB v9.1: cataloging evolutionary and functional annotations for animal, fungal, plant, archaeal, bacterial and viral orthologs.
# Zdobnov EM, Tegenfeldt F, Kuznetsov D, Waterhouse RM, Simo FA, Ioannidis P, Seppey M, Loetscher A, Kriventseva EV.
# Nucleic Acids Res. 2017 Jan 4;45(D1):D744-D749. doi: 10.1093/nar/gkw1119. Epub 2016 Nov 28. PMID: 27899580

# RUN: python OrthoDB_fetch_GENEINFO_byspecieslist.py >& fetchlog_GENEINFO_byspecieslist.txt &

import sys
from collections import defaultdict
import json
import requests ## If this produces errors, install the package
from datetime import datetime
import time

host = 'https://v100.orthodb.org/'

taxLevel = '7898' ## User-defined taxonomic level

speciesStrList = '37003_0,105023_0,48699_0,8081_0,8090_0,48701_0,48698_0,8078_0,52670_0,28743_0,8083_0,30732_0' ## User defined species list, comma separated, by taxonomic ID's from OrthoDB
numberOfSpecies = len(speciesStrList.split(','))

## User definied number of species
minNumSpecies = 2 

## deafult is 1, i.e. orthologous group must contain at least one gene from at least one of the species in the list defined above.
## to require all species in the list simply set this variable to the number of species in the list
## NB: this value must be minimum 1, and maximum the number of species in the list

## Sanity checks on user-defined number of species:
if minNumSpecies < 1:
	print('Minimum number of species must be at least 1\n')
elif minNumSpecies > numberOfSpecies:
	print('Maximum number of species must be no more than in the list: '+speciesStrList+'\n')

limit = '100000' ## this overrides the default limit of 1000

## Set the query to find all orthogroups at the specified taxonomic level
query = 'search?limit='+limit+'&level='+taxLevel

response = requests.get(host+query)
if response.status_code != 200:
	print('Bad query: '+host+query+'\n')
	print('Response code: '+str(response.status_code)+'\n')
	print('Response content: '+response.text+'\n')
	sys.exit()

## Convert JSON to Python data structure
jsonData = json.loads(response.content.decode('utf-8'))
orthoGroups = jsonData['data']

## Printing out query details
print('START: '+str(datetime.now()))
print('Your query: '+host+query)
print('Returned '+str(len(orthoGroups))+' orthologous groups\n')
print('Now retrieving gene information for '+speciesStrList.replace(',',', ')+'\n')
print('Search progress: \n')

outfile = open('ODB_GENEINFO_'+taxLevel+'.txt','w') ## File to write outputs to

## Counts to check on progress
processedGroupCount = 0
usedGroupCount = 0
geneCount = 0
headerBool = True
taxDic = defaultdict(int)

## Loop through each returned group to get gene information for genes from only the selected species
for orthoGroup in orthoGroups:
	speciesCopyDict = defaultdict(list)
	query = 'tab?id='+orthoGroup+'&species='+speciesStrList
	response = requests.get(host+query)
	if response.status_code != 200:
        	print('Bad query: '+host+query+'\n')
        	print('Response code: '+str(response.status_code)+'\n')
        	print('Response content: '+response.text+'\n')
        	sys.exit()
	
	tab = response.text
	processedGroupCount += 1
	outList = []
	for line in tab.split('\n'):
		ssline = line.strip().replace('\t','')
		if headerBool == True:
			header = ssline
			outfile.write(header+'\n')
			headerBool = False
		elif not line.strip() == '':
			columns = ssline
			outList.append(columns)
			taxDic[columns[3]] = 1
	if len(taxDic.keys()) >= minNumSpecies:
		usedGroupCount += 1
		for outLine in outList:
			outfile.write(outLine+'\n')
		geneCount += len(outList)
		if geneCount % 500 == 0:
			print(str(geneCount)+' genes retrieved from '+str(usedGroupCount)+' orthologougs groups of '+str(processedGroupCount)+' groups '+str(datetime.now()))
	if processedGroupCount % 500 == 0:
		print(str(geneCount)+' genes retrieved from '+str(usedGroupCount)+' orthologougs groups of '+str(processedGroupCount)+' groups '+str(datetime.now()))
	time.sleep(1.0)

outfile.close()

print('\nTotal genes retrieved: '+str(geneCount)+' from '+str(usedGroupCount)+' orthologous groups\n\nDONE: '+str(datetime.now()))
