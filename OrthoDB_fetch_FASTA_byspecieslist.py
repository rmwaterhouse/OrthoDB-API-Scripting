#!/usr/bin/python3
# Author: Maarten JMF Reijnders (C) 2019 maarten.reijnders@unil.ch
# Example script for downloading FASTA protein sequences from OrthoDB for a set of species using the JSON API
# NB: only sequences from genes in orthologous groups at the selected node will be retrieved with this query
# To fetch all sequences visit the flat file downloads page: http://www.orthodb.org/?page=filelist
# For further details on using the API visit: http://www.orthodb.org/?page=api
# Please cite: OrthoDB v9.1: cataloging evolutionary and functional annotations for animal, fungal, plant, archaeal, bacterial and viral orthologs.
# Zdobnov EM, Tegenfeldt F, Kuznetsov D, Waterhouse RM, Simo FA, Ioannidis P, Seppey M, Loetscher A, Kriventseva EV.
# Nucleic Acids Res. 2017 Jan 4;45(D1):D744-D749. doi: 10.1093/nar/gkw1119. Epub 2016 Nov 28. PMID: 27899580

# RUN: python OrthoDB_fetch_FASTA_byspecieslist.py >& fetchlog_FASTA_byspecieslist.txt &

import sys
from collections import defaultdict
import json
import requests ## Needs to be installed if this gives an error
from datetime import datetime
import time

host = 'https://v100.orthodb.org/'

taxLevel = '7898' ## NCBI Taxonomy ID

speciesStrList = '37003_0,105023_0,48699_0,8081_0,8090_0,48701_0,48698_0,8078_0,52670_0,28743_0,8083_0,30732_0' ## User defined list of species, comma separated, by taxonomic identifiers used in OrthoDB
numberOfSpecies = len(speciesStrList.split(',')) ## Retrieve number of species


## default is 1, i.e. orthologous group must contain at least one gene from at least one of the species in the list defined above.
## to require all species in the list simply set this variable to the number of species in the list
## NB: this value must be minimum 1, and maximum the number of species in the list
minNumSpecies = 2

## Sanity checks on user-defined number of species:
if minNumSpecies < 1:
	print('Minimum number of species must be at least 1\n')
elif minNumSpecies > numberOfSpecies:
	print('Maximum number of species must be no more than in the list: '+speciesStrList+'\n')

## Set the query to find all orthogroups at the specified taxonomic level
limit = '1000000' ## Overrides the default limit of 1000

query = 'search?limit='+limit+'&level='+taxLevel

print(host+query)
response = requests.get(host+query)
if response.status_code != 200:
	print('Bad query: '+host+query+'\n')
	print('Response code: '+str(response.status_code)+'\n')
	print('Response content: '+response.text+'\n')
	sys.exit()

## Convert JSON to Python data structure
jsonData = json.loads(response.content.decode('utf-8'))
orthoGroups = jsonData['data']

## Print out query details
print('START: '+str(datetime.now()))
print('Your query: '+host+query)
print('Returned '+str(len(orthoGroups))+' orthologous groups\n')
print('Now retrieving sequences for '+speciesStrList.replace(',',', ')+'\n')
print('Search progress: \n')

outfile = open('ODB_FASTA_'+taxLevel+'.fas','w') ## Write output to this file

## Counts used to keep track of progress
processedGroupCount = 0
usedGroupCount = 0
geneCount = 0

## Loop over every ortholog group to get the fasta sequences of the selected species
for orthoGroup in orthoGroups:
	speciesCopyDict = defaultdict(list) ## Stores fasta per species for this group, to check how many copies there are and to write to the fasta file
	query = 'fasta?id='+orthoGroup+'&species='+speciesStrList ## Query to get fastas for the species
	response = requests.get(host+query)
	if response.status_code != 200:
        	print('Bad query: '+host+query+'\n')
        	print('Response code: '+str(response.status_code)+'\n')
        	print('Response content: '+response.text+'\n')
        	sys.exit()
	
	fasta = response.text
	if not 'error' in fasta:
		for line in fasta.split('\n'): ## Split output on \n so every line is either a header or sequence. Store header + sequence as a fasta in the corresponding dictionary with species as key
			if line.startswith('>'):
				species = line.strip('>').split(':')[0]
				geneIdentifier = line.split('"pub_gene_id":"')[1].split('"')[0]
			elif not line.strip() == '':
				sequence = line.strip()
				speciesCopyDict[species].append('>'+species+'|'+orthoGroup+'|'+geneIdentifier+'\n'+sequence+'\n')
		processedGroupCount += 1
		if len(speciesCopyDict.keys()) >= minNumSpecies: ## If the amount of species with a copy of this ortholog is higher than the minimum number of species defined earlier, write sequences to outfile
				usedGroupCount += 1
				for species,genes in speciesCopyDict.items():
					for gene in genes:
						geneCount += 1
						outfile.write(gene)
						if geneCount % 500 == 0:
							print(str(geneCount)+' genes retrieved from '+str(usedGroupCount)+' orthologous groups of '+str(processedGroupCount)+' groups '+str(datetime.now()))
							sys.exit()
		if processedGroupCount % 500 == 0:
			print(str(geneCount)+' genes retrieved from '+str(usedGroupCount)+' orthologous groups of '+str(processedGroupCount)+' groups '+str(datetime.now()))
		time.sleep(0.1)
	else:
		print('Empty fasta')
outfile.close()

print('\n Total genes retrieved: '+str(geneCount)+' from '+str(usedGroupCount)+' orthologous groups\n\nDONE: '+str(datetime.now()))
