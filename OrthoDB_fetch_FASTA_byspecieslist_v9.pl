#!/usr/local/bin/perl
# Author: Robert M. Waterhouse (C) 2019 robert.waterhouse@unil.ch
# Example script for downloading FASTA protein sequences from OrthoDB for a set of species using the JSON API
# NB: only sequences from genes in orthologous groups at the selected node will be retrieved with this query
# To fetch all sequences visit the flat file downloads page: http://www.orthodb.org/?page=filelist
# For further details on using the API visit: http://www.orthodb.org/?page=api
# Please cite: OrthoDB v9.1: cataloging evolutionary and functional annotations for animal, fungal, plant, archaeal, bacterial and viral orthologs.
# Zdobnov EM, Tegenfeldt F, Kuznetsov D, Waterhouse RM, Simo FA, Ioannidis P, Seppey M, Loetscher A, Kriventseva EV.
# Nucleic Acids Res. 2017 Jan 4;45(D1):D744-D749. doi: 10.1093/nar/gkw1119. Epub 2016 Nov 28. PMID: 27899580

# RUN: perl OrthoDB_fetch_FASTA_byspecieslist_v9.pl >& fetchlog_FASTA_byspecieslist_v9.txt &

use strict;
use warnings;

###===###
# Make sure these required modules are installed:
use REST::Client;    # for database connection and querying
use JSON;            # for JSON to Perl interoperability

###===###
# Set connection to the OrthoDB website:
my $host = 'http://www.orthodb.org/v9/';
my $client = REST::Client->new(host => $host);

###===###
# User-defined taxonomic level:
my $level=50557;          # e.g. 50557 = NCBI taxonomy ID for the node Insecta

###===###
# User-defined list of species:
my @species=qw(136037 7165 7070);
# e.g. 136037 = NCBI taxonomy ID for Zootermopsis nevadensis
# e.g.   7165 = NCBI taxonomy ID for Anopheles gambiae
# e.g.   7070 = NCBI taxonomy ID for Tribolium castaneum
# NB1: if the species in not inculuded in OrthoDB then clearly no results will be returned.
# NB2: if the species is not a member of the taxonomic level defined above then clearly no results will be returned.
# NB3: entering an NCBI taxonomy ID for a taxonomic level (must be present at OrthoDB) instead
# of an individual species is equivalent to entering the full list of species at that level.

###===###
# User-defined number of species:
my $min_num_spe=2;
# deafult is 1, i.e. orthologous group must contain at least one gene from at least one of the species in the list defined above.
# to require all species in the list simply set this variable to the number of species in the list
# NB: this value must be minimum 1, and maximum the number of species in the list
# Sanity checks on user-defined number of species:
if($min_num_spe<1) { print "Minimum number of species must be at least 1\n"; exit(); }  
if($min_num_spe>scalar(@species)) { print "Maximum number of species must be no more than in the list (" . scalar(@species) . ")\n"; exit(); }  

###===###
# Set the query to find all orthogroups at the specified taxonomic level
my $limit=1000000;            # this overrides the default limit of 1000
$client->GET("search?limit=$limit&level=$level");    
if($client->responseCode() !=200) { 
    print "Bad query: http://www.orthodb.org/search?limit=$limit&level=$level\n"; 
    print "Response Code: " . $client->responseCode() . "\n";
    print "Response Content: " . $client->responseContent() . "\n";
    exit(); 
}

###===###
# Convert JSON to PERL data structure
my $result = decode_json( $client->responseContent() );

###===###
# Get all orthologous groups returned from the query
my @groups = @{ $result->{'data'} };

###===###
# Check query details
print "START: " . `date`;
print "Your query: http://www.orthodb.org/search?limit=$limit&level=$level\n";
print "Returned " . scalar(@groups) . " orthologous groups\n\n";
print "Now retrieving sequences for " . join(", ", @species) . "\n";
print "Search progress:\n";

###===###
# Loop through each returned group to get FASTA protein sequences for genes from only the selected species
# Open output FASTA file
open(OUT,">ODB_FASTA\_$level\.fas") || die $!;
my $specieslist=join(",",@species);
my $genecount=0;
my $groupcount=0;
my $groupprog=0;
my $progressc=0;
my $orthogrpc=0;
my $sleepskip=0;
foreach my $group (@groups) {
    $client->GET("fasta?id=$group&species=$specieslist");
    if($client->responseCode() !=200) { 
	print "Bad query: http://www.orthodb.org/fasta?id=$group&species=$specieslist\n";  
	print "Response Code: " . $client->responseCode() . "\n";
	print "Response Content: " . $client->responseContent() . "\n";
	exit(); 
    }
    $groupcount++;
    $groupprog++;
    if($client->responseContent()) { 
	my $fasta=$client->responseContent();
	my @fastalines=split(/\n/,$fasta);
	undef my %taxids;
	foreach my $fastaline (@fastalines) {
	    if($fastaline=~/^>(\S+):\S+\s+/) { $taxids{$1}=1; }
	}
	if(scalar(keys %taxids)>=$min_num_spe) {
	    my @genes = $fasta =~ /$group/g;
	    $genecount+=scalar(@genes);
	    $progressc+=scalar(@genes);
	    $orthogrpc++;
	    $fasta=~s/\{//g;
	    $fasta=~s/\}//g;
	    print OUT $fasta;
	    if($progressc>500) { 
		print "$genecount genes retrieved from $orthogrpc orthologous groups of $groupcount groups " . `date`;
		$progressc=0; 
	    }
	}
    }
    if($groupprog==500) { 
	print "$genecount genes retrieved from $orthogrpc orthologous groups of $groupcount groups " . `date`;
	$groupprog=0; 
    }
    $sleepskip++;
    if($sleepskip>10) {      # edit here to change how often and/or how long to pause between sets of queries
	my $cmd="sleep 1";   # pause between sets of queries to avoid bombarding OrthoDB with too many
	system($cmd);        # queries which could result in being locked out and disconnected
	$sleepskip=0;
    }
}
close(OUT);
print "\nTotal genes retrieved: $genecount from $orthogrpc orthologous groups\n\nDONE: " . `date`;
