#!/usr/bin/python3
# Author: Maarten JMF. Reijnders (C) 2019 maarten.reijnders@unil.ch
# Example script for downloading FASTA protein sequences from OrthoDB for a set of species using the JSON API
# NB: only sequences from genes in orthologous groups at the selected node will be retrieved with this query
# To fetch all sequences visit the flat file downloads page: http://www.orthodb.org/?page=filelist
# For further details on using the API visit: http://www.orthodb.org/?page=api
# Please cite: OrthoDB v9.1: cataloging evolutionary and functional annotations for animal, fungal, plant, archaeal, bacterial and viral orthologs.
# Zdobnov EM, Tegenfeldt F, Kuznetsov D, Waterhouse RM, Simo FA, Ioannidis P, Seppey M, Loetscher A, Kriventseva EV.
# Nucleic Acids Res. 2017 Jan 4;45(D1):D744-D749. doi: 10.1093/nar/gkw1119. Epub 2016 Nov 28. PMID: 27899580

# RUN: python OrthoDB_fetch_OGs_FASTA_byspecieslist_v9.py >& fetchlog_OGs_FASTA_byspecieslist_v9.txt

import sys
from collections import defaultdict
import json
import requests ## If this produces an error you need to install it
from datetime import datetime
import time

host = 'http://www.orthodb.org/v9/' ## OrthoDB website

taxLevel = '7147' ## NCBI Taxonomy ID for the node Diptera. Edit with your own prefered Tax ID

## Dictionary with our selected species. Key (first value) = numerical Taxonomic identifier from OrthoDB, value (second value) = short name that you can give yourself
speciesDict = {'7165': 'Agamb',
		'7176': 'Cquin',
		'7222': 'Dgrim',
		'7227': 'Dmela',
		'7394': 'Gmors'}

speciesStrList = ','.join(speciesDict.keys())

## Required profiles for our orthologs groups. Key = taxonomic identifier, value = list with at first value minimum and second value maximum amount of copies. E.g. our selected species need to be single copy [1,1], and some other selected species need to be either single copy or not have the ortholog ([0,1]). Again, edit for user preference
profileDict = {7165: [1,1],
		7176: [1,1],
		7222: [1,1],
		7227: [1,1],
		7394: [1,1],
		7159: [0,1],
		7167: [0,1],
		7213: [0,1],
		7237: [0,1],
		39758: [0,1]}

limit = '100' ## Limit for the query. Set to low limit when testing the script
query = 'search?limit='+limit+'&level='+taxLevel ## Query to retrieve all orthologs at the taxonomic level

response = requests.get(host+query) ## This queries OrthoDB with our query

## This little snippet lets us know if the query produces an error, and stops the script when an error is found. Seen multiple times throughout the code
if response.status_code != 200:
	print('Bad query: '+host+query+'\n')
	print('Response code: '+str(response.status_code)+'\n')
	print('Response content: '+response.text+'\n')
	sys.exit()

jsonData = json.loads(response.content.decode('utf-8')) ## Convert our query response to json
orthoGroups = jsonData['data'] ## Retrieve the part from JSON we're interested in

## Prints out query information
print('START: '+str(datetime.now()))
print('Your query: '+host+query)
print('Returned '+str(len(orthoGroups))+' orthologous groups\n')
print('Now searching for groups that match the following profile:\nTaxID\tCopy-number\n')
for taxID in sorted(profileDict):
	if str(taxID) in speciesDict:
		print(str(taxID)+'\t'+str(profileDict[taxID])+'\tSELECTED => '+speciesDict[str(taxID)])
	else:
		print(str(taxID)+'\t'+str(profileDict[taxID]))

print('\nSearch progress: \n')

## File to print fasta results from selected species
fastaOutfile = open(taxLevel+'_profile_selected.fas','w')
## File to print ortholog group IDs of groups that match the profile
lstOutfile = open(taxLevel+'_profile_selected.txt','w')
## File to print ortholog group IDs of groups that don't match the profile
nonOutfile = open(taxLevel+'_profile_rejected.txt','w')
## File to print additional information for the genes belonging to orthologs that match the profile
annOutfile = open(taxLevel+'_profile_geneinfo.txt','w')

## Several counts to keep track of progress 
orthoGroupCount = 0
geneCount = 0
processedGroupCounts = 0
genToDes = {}
for orthoGroup in orthoGroups: ## Loops over every orthologous group we retrieved from OrthoDB
	profileMatchCount = 0 ## Profile match counter. Every profile match from the selected species adds 1. Having the same amount of profilematches as there are species in the selected species means this ortholog group matches all our requirements for the selected species
	speciesFastaDict = defaultdict(list) ## Dictionary to hold fastas belonging to species
	query = 'fasta?id='+orthoGroup+'&species='+speciesStrList ## Query to get all fastas belonging to orthologous groups for our species in the profile dictionary
	response = requests.get(host+query) ## Query OrthoDB
	if response.status_code != 200:
		print('Bad query: '+host+query+'\n')
		print('Response code: '+str(response.status_code)+'\n')
		print('Response content: '+response.text+'\n')
		sys.exit()

	fastas = response.text
	profileMatch = True ## Boolean that will keep track if all profile requirements are met or not. Once one requirement is violated, boolean will be False
	fastaPrint = '' ## This variable will hold all the fasta information we potentially want to output to a file
	geneInfo = '' ## This variable will hold all gene information we potentially want to output to a file
	for line in fastas.split('\n'): ## Loop over the fasta file. Split by \n (= line break) so that every line is either a header or a sequence. Store fasta in 'speciesFastaDict' by species
		if line.startswith('>'):
			species = line.strip().split(':')[0].strip('>')
			geneID = line.strip().split(' ')[0].strip('>').split('\n')[0].split('|')[0]
			description = line.strip().split(' ',1)[1]
			genToDes[geneID] = description
			header = '>'+geneID+'|'+species
		else:
			sequence = line.strip()
			if species in speciesDict and not sequence == '':
				speciesFastaDict[species].append(header+'\n'+sequence)
	orgGeneCount = 0
	for species,fastas in speciesFastaDict.items(): ## Loop over all species to count the fasta sequences and see if they number matches the profile requirements. If it matches, add fasta sequences to the 'fastaPrint' variable
		requiredCopyProfile = profileDict[int(species)]
		requiredCopyProfileMin = requiredCopyProfile[0]
		requiredCopyProfileMax = requiredCopyProfile[1]
		if len(fastas) >= requiredCopyProfileMin and len(fastas) <= requiredCopyProfileMax: ## If the number of fasta sequences for this species orthologs is higher or equal to the min and lower or equal to the max requirements, profileMatch remains True. Otherwise profileMatch = False and go to the next ortholog group (break this current loop)
			profileMatch = True
		else:
			profileMatch = False
			break
		if species in speciesDict: ## If profileMatch = True and its species are in the selected species, profileMatch count += 1 so we can check later if all selected species match the profile
			if profileMatch == True:
				profileMatchCount += 1
			for fasta in fastas: ## This loop gets all additional gene info for the sequence and appends it to the 'geneInfo' variable so we can potentially output it later
				geneID = fasta.split(' ')[0].strip('>').split('\n')[0].split('|')[0]
				sequence = fasta.split('\n')[1]
				query = 'ogdetails?id='+geneID
				response = requests.get(host+query)
				if response.status_code != 200:
					print('Bad query: '+host+query+'\n')
					print('Response code: '+str(response.status_code)+'\n')
					print('Response content: '+response.text+'\n')
					sys.exit()
				geneInfo += orthoGroup+'\t'+speciesDict[species]+'\t'+geneID+'\t'+response.text+'\n'
				if not sequence.startswith('{'): ## Sometimes OrthoDB has a gene as part of an ortholog group but not an actual sequence. If this happens, disregard the ortholog group because it wont match our requirements
					fastaPrint += '>'+geneID+' '+speciesDict[species]+'\t'+orthoGroup+'\t'+genToDes[geneID]+'\n'+sequence+'\n'
				else:
					profileMatch = False
					break
		orgGeneCount += len(fastas)
	if profileMatch == True and profileMatchCount == len(speciesDict): ## If the profileMatch is still True and the profileMatchCount equals the amount of selected species, write everything to the appropriate out files
		fastaOutfile.write(fastaPrint)
		lstOutfile.write(orthoGroup+'\n')
		annOutfile.write(geneInfo+'\n')
		geneCount += orgGeneCount
		orthoGroupCount += 1
	else:
		nonOutfile.write(orthoGroup+'\n') ## If it doesn't meet the requirements, write ortholog group to outfile
	processedGroupCounts += 1
	#if geneCount % 500 == 0:
	print(str(geneCount)+' genes retrieved from '+str(orthoGroupCount)+' orthologous groups of '+str(processedGroupCounts)+' groups '+str(datetime.now()))
	time.sleep(0.1)

fastaOutfile.close()
